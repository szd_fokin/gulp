$(document).ready(function () {
    
    var $buttons = $('.js-button-click');
    
    $buttons.each(function () {
        
        var $button = $(this);
        $button.click(function () {
            if (!$(this).hasClass('has-click')) {
                $buttons.removeClass('has-click');
                $(this).addClass('has-click');
                alert($button.attr('class'));
            }
        });
        
    });
    
});